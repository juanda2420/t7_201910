package test;

import data_structures.RedBlackBalancedSearchTrees;
import junit.framework.TestCase;

public class testRB extends TestCase {

	private RedBlackBalancedSearchTrees<Integer, String> arbol = new RedBlackBalancedSearchTrees<Integer, String>();

	/**
	 * Arreglo con los elementos del escenario
	 */
	protected static final String[] ADRESS_ID = { "350", "383", "105", "233", "140", "266", "356", "236", "80", "360",
			"221", "241", "130", "244", "352", "446", "18", "98", "97" };
	protected static final Integer[] LLAVES = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };

	public void setupEscenario1() {

		for (int i = 0; i < ADRESS_ID.length; i++) {

			String valueAct = ADRESS_ID[i];
			int keyAct = LLAVES[i];

			arbol.put(keyAct, valueAct);

		}

	}

	public void testPut() throws Exception {
		// Prueba la lista vac�a.
		assertTrue("La tabla debe estar vacia", arbol.isEmpty());
		assertEquals("no es 0", 0, arbol.size());

		// Agrega dos elementos.

		arbol.put(91384398, "nuevo");
		arbol.put(45349549, "holi");
		assertFalse("La lista no deberia estar vacia", arbol.isEmpty());
		assertEquals("Debe contener 2 elementos", 2, arbol.size());

		// Agrega 20 elementos.
		arbol.delete(91384398);
		arbol.delete(45349549);
		setupEscenario1();

		assertFalse("La lista no es vacia", arbol.isEmpty());
		assertEquals("La lista debe tener 20 elementos", ADRESS_ID.length, arbol.size());

	}

	public void testDelete() throws Exception {

		// Prueba la lista vac�a.
		assertTrue("La tabla debe estar vacia", arbol.isEmpty());
		assertEquals("no es 0", 0, arbol.size());

		// Agrega dos elementos.

		arbol.put(91384398, "nuevo");
		arbol.put(45349549, "holi");
		assertFalse("La lista no deberia estar vacia", arbol.isEmpty());
		assertEquals("Debe contener 2 elementos", 2, arbol.size());

		// Elimina los 2 elementos

		arbol.delete(91384398);
		arbol.delete(45349549);
		assertTrue("La tabla debe estar vacia", arbol.isEmpty());

		// Carga los 20 elementos
		setupEscenario1();

	}

	public void testContains() throws Exception {

		// Prueba la lista vac�a.
		assertTrue("La tabla debe estar vacia", arbol.isEmpty());
		assertEquals("no es 0", 0, arbol.size());

		// Agrega dos elementos.

		arbol.put(91384398, "nuevo");
		arbol.put(45349549, "holi");
		assertFalse("La lista no deberia estar vacia", arbol.isEmpty());
		assertEquals("Debe contener 2 elementos", 2, arbol.size());

		// Elimina los 2 elementos

		assertTrue("La tabla debe contener la llave dada", arbol.contains(91384398));
		assertFalse("La tabla  NO debe contener la llave dada", arbol.contains(91386698));

		arbol.put(123456, "nuevo2");
		assertEquals(true, arbol.contains(123456));

		// Carga los 20 elementos
		setupEscenario1();

	}

	public void testGet() throws Exception {

		// Prueba la lista vac�a.
		assertTrue("La tabla debe estar vacia", arbol.isEmpty());
		assertEquals("no es 0", 0, arbol.size());

		// Agrega dos elementos.

		arbol.put(91384398, "nuevo");
		arbol.put(45349549, "holi");
		assertFalse("La lista no deberia estar vacia", arbol.isEmpty());
		assertEquals("Debe contener 2 elementos", 2, arbol.size());

		// Elimina los 2 elementos

		assertEquals("nuevo", arbol.get(91384398));
		assertEquals("holi", arbol.get(45349549));
		assertNull(arbol.get(123456789));

	}

	public void testHeight() {

		setupEscenario1();

		assertEquals(4, arbol.height());

	}

	public void isEmpty() {

		assertEquals(true, arbol.isEmpty());
		// Se agregan elementos
		setupEscenario1();
		assertEquals(false, arbol.isEmpty());

	}

	public void testGetSize() {
		// Prueba la lista vac�a.
		assertEquals("El tamaño de la lista vacia no es correcto", 0, arbol.size());

		// Prueba la lista con dos elementos

		arbol.put(91384398, "nuevo");
		arbol.put(45349549, "holi");

		assertEquals("El tama�o de la lista con dos elementos no es correcto", 2, arbol.size());

		arbol.delete(91384398);
		arbol.delete(45349549);

		// Prueba la lista con 20 elementos
		setupEscenario1();
		assertEquals("El tama�o de la lista con 20 elementos no es correcto", LLAVES.length, arbol.size());

	}

}
