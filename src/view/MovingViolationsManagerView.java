package view;

import java.util.ArrayList;

import data_structures.IQueue;
import data_structures.Queue;
import vo.VOMovingViolations;

public class MovingViolationsManagerView 
{
	/**
	 * Constante con el número maximo de datos maximo que se deben imprimir en consola
	 */
	public static final int N = 20;

	public void printMenu() 
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("-------------------Taler 6--------------------");
		System.out.println("0. Cargar las infracciones de enero a junio");
		System.out.println("1. Consultar la informacion asociada a un ObjectId.");
		System.out.println("2. Consultar los ObjectId's de las infracciones que se encuentran en un rango dado.");
		System.out.println("3. Salir");
		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}

	public void printMessage(String mensaje) 
	{
		System.out.println(mensaje);
	}

	public void printReq1(VOMovingViolations infraccion)
	{
		if(infraccion != null)
		{

			this.printMessage("Location: " + infraccion.getLocation() + " | AddressId: " +infraccion.getAdress_ID() + 
					" | StreetSegid: "+ infraccion.getStreetSEID() + " | XCoord: "+infraccion.getXcoord() + 
					" | YCoord: "+infraccion.getYcoord() + " | TicketIssueDate: " + infraccion.getTicketIssueDate());

		}
		else
		{
			this.printMessage("La infraccion no es valida");
		}
	}

	public void printReq2(IQueue<VOMovingViolations> arreglo)
	{
		if(arreglo.size() != 0)
		{
			for(VOMovingViolations act : arreglo)
			{
				this.printMessage("Object ID: "+ act.objectId() + " | Location: " + act.getLocation() + 
						" | AddressId: " +act.getAdress_ID() + " | StreetSegid: "+ act.getStreetSEID() + 
						" | XCoord: "+act.getXcoord() + " | YCoord: "+act.getYcoord() + 
						" | TicketIssueDate: " + act.getTicketIssueDate());
			}
		}
		else
		{
			this.printMessage("Lista vacia");
		}
	}

}
