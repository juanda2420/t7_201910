package logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import data_structures.IQueue;
import data_structures.Queue;
import data_structures.RedBlackBalancedSearchTrees;
import vo.VOMovingViolations;

public class MovingViolationsManager {


	RedBlackBalancedSearchTrees<Integer, VOMovingViolations> arbolRB = new RedBlackBalancedSearchTrees<Integer, VOMovingViolations>();
	
	int cJanuary = 0;
	int cFebruary = 0;
	int cMarch = 0;
	int cApril = 0;
	int cMay = 0;
	int cJune = 0;

	public void loadJanuary(String archivoJanuary) throws FileNotFoundException, IOException, ParseException {

		JSONParser parser = new JSONParser();

		try {
			JSONArray a = (JSONArray) parser.parse(new FileReader(archivoJanuary));

			for (Object o : a) 
			{
				
				JSONObject actual = (JSONObject) o;
				
				String OBJECTID;
				String ROW;
				String LOCATION;
				String ADDRESS_ID;
				String STREETSEGID;
				String XCOORD;
				String YCOORD;
				String TICKETTYPE;
				String FINEAMT;
				String TOTALPAID;
				String PENALTY1;
				String PENALTY2;
				String ACCIDENTINDICATOR;
				String TICKETISSUEDATE;
				String VIOLATIONCODE;
				String VIOLATIONDESC;
				
				if (actual.get("OBJECTID")==null) {
					OBJECTID = "-1";
				}
				else {
				 OBJECTID = String.valueOf(actual.get("OBJECTID"));
				}
				
				if (actual.get("ROW_")==null) {
					ROW = "-1";
				}
				else {
					 ROW = String.valueOf(actual.get("ROW_"));
				}
				
				if (actual.get("LOCATION")==null) {
					LOCATION = "-1";
				}
				else {
					 LOCATION = String.valueOf(actual.get("LOCATION"));
				}
				
				if (actual.get("ADDRESS_ID")==null) {
					ADDRESS_ID = "-1";
				}
				else {
					ADDRESS_ID = String.valueOf(actual.get("ADDRESS_ID"));
				}
				
				if (actual.get("STREETSEGID")==null) {
					STREETSEGID = "-1";
				}
				else {
					STREETSEGID = String.valueOf(actual.get("STREETSEGID"));
				}
				
				if (actual.get("XCOORD")==null) {
					XCOORD = "-1";
				}
				else {
					 XCOORD = String.valueOf(actual.get("XCOORD"));
				}
				
				if (actual.get("YCOORD")==null) {
					YCOORD = "-1";
				}
				else {
					 YCOORD = String.valueOf(actual.get("YCOORD"));
				}
				
				if (actual.get("TICKETTYPE")==null) {
					TICKETTYPE = "-1";
				}
				else {
					TICKETTYPE = String.valueOf(actual.get("TICKETTYPE"));
				}
				
				if (actual.get("FINEAMT")==null) {
					FINEAMT = "-1";
				}
				else {
					 FINEAMT = String.valueOf(actual.get("FINEAMT"));
				}
				
				if (actual.get("TOTALPAID")==null) {
					TOTALPAID = "-1";
				}
				else {
					TOTALPAID = String.valueOf(actual.get("TOTALPAID"));
				}
				
				if (actual.get("PENALTY1")==null) {
					PENALTY1 = "-1";
				}
				else {
					 PENALTY1 = String.valueOf(actual.get("PENALTY1"));
				}
				
				if (actual.get("PENALTY2")==null) {
					PENALTY2 = "-1";
				}
				else {
					PENALTY2 = String.valueOf(actual.get("PENALTY2"));
				}
				if (actual.get("ACCIDENTINDICATOR")==null) {
					ACCIDENTINDICATOR = "-1";
				}
				else {
					ACCIDENTINDICATOR = String.valueOf(actual.get("ACCIDENTINDICATOR"));
				}
				
				if (actual.get("TICKETISSUEDATE")==null) {
					TICKETISSUEDATE = "-1";
				}
				else {
					 TICKETISSUEDATE = String.valueOf(actual.get("TICKETISSUEDATE"));
				}
				
				if (actual.get("VIOLATIONCODE")==null) {
					VIOLATIONCODE = "-1";
				}
				else {
					VIOLATIONCODE = String.valueOf(actual.get("VIOLATIONCODE"));
				}
				
				if (actual.get("VIOLATIONDESC")==null) {
					VIOLATIONDESC = "-1";
				}
				else {
					 VIOLATIONDESC = String.valueOf(actual.get("VIOLATIONDESC"));
				}

				VOMovingViolations nuevo = new VOMovingViolations(OBJECTID, ROW, LOCATION, ADDRESS_ID, STREETSEGID, XCOORD,
						YCOORD, TICKETTYPE, FINEAMT, TOTALPAID, PENALTY1, PENALTY2, ACCIDENTINDICATOR, TICKETISSUEDATE, VIOLATIONCODE, VIOLATIONDESC);
				cJanuary++;

				arbolRB.put(nuevo.objectId(), nuevo);

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public void loadFebruary(String archivoFebruary) {
		JSONParser parser = new JSONParser();

		try {
			JSONArray a = (JSONArray) parser.parse(new FileReader(archivoFebruary));

			for (Object o : a) {

				JSONObject actual = (JSONObject) o;
				
				String OBJECTID;
				String ROW;
				String LOCATION;
				String ADDRESS_ID;
				String STREETSEGID;
				String XCOORD;
				String YCOORD;
				String TICKETTYPE;
				String FINEAMT;
				String TOTALPAID;
				String PENALTY1;
				String PENALTY2;
				String ACCIDENTINDICATOR;
				String TICKETISSUEDATE;
				String VIOLATIONCODE;
				String VIOLATIONDESC;
				
				if (actual.get("OBJECTID")==null) {
					OBJECTID = "-1";
				}
				else {
				 OBJECTID = String.valueOf(actual.get("OBJECTID"));
				}
				
				if (actual.get("ROW_")==null) {
					ROW = "-1";
				}
				else {
					 ROW = String.valueOf(actual.get("ROW_"));
				}
				
				if (actual.get("LOCATION")==null) {
					LOCATION = "-1";
				}
				else {
					 LOCATION = String.valueOf(actual.get("LOCATION"));
				}
				
				if (actual.get("ADDRESS_ID")==null) {
					ADDRESS_ID = "-1";
				}
				else {
					ADDRESS_ID = String.valueOf(actual.get("ADDRESS_ID"));
				}
				
				if (actual.get("STREETSEGID")==null) {
					STREETSEGID = "-1";
				}
				else {
					STREETSEGID = String.valueOf(actual.get("STREETSEGID"));
				}
				
				if (actual.get("XCOORD")==null) {
					XCOORD = "-1";
				}
				else {
					 XCOORD = String.valueOf(actual.get("XCOORD"));
				}
				
				if (actual.get("YCOORD")==null) {
					YCOORD = "-1";
				}
				else {
					 YCOORD = String.valueOf(actual.get("YCOORD"));
				}
				
				if (actual.get("TICKETTYPE")==null) {
					TICKETTYPE = "-1";
				}
				else {
					TICKETTYPE = String.valueOf(actual.get("TICKETTYPE"));
				}
				
				if (actual.get("FINEAMT")==null) {
					FINEAMT = "-1";
				}
				else {
					 FINEAMT = String.valueOf(actual.get("FINEAMT"));
				}
				
				if (actual.get("TOTALPAID")==null) {
					TOTALPAID = "-1";
				}
				else {
					TOTALPAID = String.valueOf(actual.get("TOTALPAID"));
				}
				
				if (actual.get("PENALTY1")==null) {
					PENALTY1 = "-1";
				}
				else {
					 PENALTY1 = String.valueOf(actual.get("PENALTY1"));
				}
				
				if (actual.get("PENALTY2")==null) {
					PENALTY2 = "-1";
				}
				else {
					PENALTY2 = String.valueOf(actual.get("PENALTY2"));
				}
				if (actual.get("ACCIDENTINDICATOR")==null) {
					ACCIDENTINDICATOR = "-1";
				}
				else {
					ACCIDENTINDICATOR = String.valueOf(actual.get("ACCIDENTINDICATOR"));
				}
				
				if (actual.get("TICKETISSUEDATE")==null) {
					TICKETISSUEDATE = "-1";
				}
				else {
					 TICKETISSUEDATE = String.valueOf(actual.get("TICKETISSUEDATE"));
				}
				
				if (actual.get("VIOLATIONCODE")==null) {
					VIOLATIONCODE = "-1";
				}
				else {
					VIOLATIONCODE = String.valueOf(actual.get("VIOLATIONCODE"));
				}
				
				if (actual.get("VIOLATIONDESC")==null) {
					VIOLATIONDESC = "-1";
				}
				else {
					 VIOLATIONDESC = String.valueOf(actual.get("VIOLATIONDESC"));
				}

				VOMovingViolations nuevo = new VOMovingViolations(OBJECTID, ROW, LOCATION, ADDRESS_ID, STREETSEGID, XCOORD,
						YCOORD, TICKETTYPE, FINEAMT, TOTALPAID, PENALTY1, PENALTY2, ACCIDENTINDICATOR, TICKETISSUEDATE, VIOLATIONCODE, VIOLATIONDESC);
				cFebruary++;
				arbolRB.put(Integer.parseInt(OBJECTID), nuevo);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public void loadMarch(String archivoMarch) {
		JSONParser parser = new JSONParser();

		try {
			JSONArray a = (JSONArray) parser.parse(new FileReader(archivoMarch));

			for (Object o : a) {

				JSONObject actual = (JSONObject) o;
				
				String OBJECTID;
				String ROW;
				String LOCATION;
				String ADDRESS_ID;
				String STREETSEGID;
				String XCOORD;
				String YCOORD;
				String TICKETTYPE;
				String FINEAMT;
				String TOTALPAID;
				String PENALTY1;
				String PENALTY2;
				String ACCIDENTINDICATOR;
				String TICKETISSUEDATE;
				String VIOLATIONCODE;
				String VIOLATIONDESC;
				
				if (actual.get("OBJECTID")==null) {
					OBJECTID = "-1";
				}
				else {
				 OBJECTID = String.valueOf(actual.get("OBJECTID"));
				}
				
				if (actual.get("ROW_")==null) {
					ROW = "-1";
				}
				else {
					 ROW = String.valueOf(actual.get("ROW_"));
				}
				
				if (actual.get("LOCATION")==null) {
					LOCATION = "-1";
				}
				else {
					 LOCATION = String.valueOf(actual.get("LOCATION"));
				}
				
				if (actual.get("ADDRESS_ID")==null) {
					ADDRESS_ID = "-1";
				}
				else {
					ADDRESS_ID = String.valueOf(actual.get("ADDRESS_ID"));
				}
				
				if (actual.get("STREETSEGID")==null) {
					STREETSEGID = "-1";
				}
				else {
					STREETSEGID = String.valueOf(actual.get("STREETSEGID"));
				}
				
				if (actual.get("XCOORD")==null) {
					XCOORD = "-1";
				}
				else {
					 XCOORD = String.valueOf(actual.get("XCOORD"));
				}
				
				if (actual.get("YCOORD")==null) {
					YCOORD = "-1";
				}
				else {
					 YCOORD = String.valueOf(actual.get("YCOORD"));
				}
				
				if (actual.get("TICKETTYPE")==null) {
					TICKETTYPE = "-1";
				}
				else {
					TICKETTYPE = String.valueOf(actual.get("TICKETTYPE"));
				}
				
				if (actual.get("FINEAMT")==null) {
					FINEAMT = "-1";
				}
				else {
					 FINEAMT = String.valueOf(actual.get("FINEAMT"));
				}
				
				if (actual.get("TOTALPAID")==null) {
					TOTALPAID = "-1";
				}
				else {
					TOTALPAID = String.valueOf(actual.get("TOTALPAID"));
				}
				
				if (actual.get("PENALTY1")==null) {
					PENALTY1 = "-1";
				}
				else {
					 PENALTY1 = String.valueOf(actual.get("PENALTY1"));
				}
				
				if (actual.get("PENALTY2")==null) {
					PENALTY2 = "-1";
				}
				else {
					PENALTY2 = String.valueOf(actual.get("PENALTY2"));
				}
				if (actual.get("ACCIDENTINDICATOR")==null) {
					ACCIDENTINDICATOR = "-1";
				}
				else {
					ACCIDENTINDICATOR = String.valueOf(actual.get("ACCIDENTINDICATOR"));
				}
				
				if (actual.get("TICKETISSUEDATE")==null) {
					TICKETISSUEDATE = "-1";
				}
				else {
					 TICKETISSUEDATE = String.valueOf(actual.get("TICKETISSUEDATE"));
				}
				
				if (actual.get("VIOLATIONCODE")==null) {
					VIOLATIONCODE = "-1";
				}
				else {
					VIOLATIONCODE = String.valueOf(actual.get("VIOLATIONCODE"));
				}
				
				if (actual.get("VIOLATIONDESC")==null) {
					VIOLATIONDESC = "-1";
				}
				else {
					 VIOLATIONDESC = String.valueOf(actual.get("VIOLATIONDESC"));
				}

				VOMovingViolations nuevo = new VOMovingViolations(OBJECTID, ROW, LOCATION, ADDRESS_ID, STREETSEGID, XCOORD,
						YCOORD, TICKETTYPE, FINEAMT, TOTALPAID, PENALTY1, PENALTY2, ACCIDENTINDICATOR, TICKETISSUEDATE, VIOLATIONCODE, VIOLATIONDESC);
				cMarch++;
				arbolRB.put(Integer.parseInt(OBJECTID), nuevo);

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public void loadApril(String archivoApril) {
		JSONParser parser = new JSONParser();

		try {
			JSONArray a = (JSONArray) parser.parse(new FileReader(archivoApril));

			for (Object o : a) {

				JSONObject actual = (JSONObject) o;
				
				String OBJECTID;
				String ROW;
				String LOCATION;
				String ADDRESS_ID;
				String STREETSEGID;
				String XCOORD;
				String YCOORD;
				String TICKETTYPE;
				String FINEAMT;
				String TOTALPAID;
				String PENALTY1;
				String PENALTY2;
				String ACCIDENTINDICATOR;
				String TICKETISSUEDATE;
				String VIOLATIONCODE;
				String VIOLATIONDESC;
				
				if (actual.get("OBJECTID")==null) {
					OBJECTID = "-1";
				}
				else {
				 OBJECTID = String.valueOf(actual.get("OBJECTID"));
				}
				
				if (actual.get("ROW_")==null) {
					ROW = "-1";
				}
				else {
					 ROW = String.valueOf(actual.get("ROW_"));
				}
				
				if (actual.get("LOCATION")==null) {
					LOCATION = "-1";
				}
				else {
					 LOCATION = String.valueOf(actual.get("LOCATION"));
				}
				
				if (actual.get("ADDRESS_ID")==null) {
					ADDRESS_ID = "-1";
				}
				else {
					ADDRESS_ID = String.valueOf(actual.get("ADDRESS_ID"));
				}
				
				if (actual.get("STREETSEGID")==null) {
					STREETSEGID = "-1";
				}
				else {
					STREETSEGID = String.valueOf(actual.get("STREETSEGID"));
				}
				
				if (actual.get("XCOORD")==null) {
					XCOORD = "-1";
				}
				else {
					 XCOORD = String.valueOf(actual.get("XCOORD"));
				}
				
				if (actual.get("YCOORD")==null) {
					YCOORD = "-1";
				}
				else {
					 YCOORD = String.valueOf(actual.get("YCOORD"));
				}
				
				if (actual.get("TICKETTYPE")==null) {
					TICKETTYPE = "-1";
				}
				else {
					TICKETTYPE = String.valueOf(actual.get("TICKETTYPE"));
				}
				
				if (actual.get("FINEAMT")==null) {
					FINEAMT = "-1";
				}
				else {
					 FINEAMT = String.valueOf(actual.get("FINEAMT"));
				}
				
				if (actual.get("TOTALPAID")==null) {
					TOTALPAID = "-1";
				}
				else {
					TOTALPAID = String.valueOf(actual.get("TOTALPAID"));
				}
				
				if (actual.get("PENALTY1")==null) {
					PENALTY1 = "-1";
				}
				else {
					 PENALTY1 = String.valueOf(actual.get("PENALTY1"));
				}
				
				if (actual.get("PENALTY2")==null) {
					PENALTY2 = "-1";
				}
				else {
					PENALTY2 = String.valueOf(actual.get("PENALTY2"));
				}
				if (actual.get("ACCIDENTINDICATOR")==null) {
					ACCIDENTINDICATOR = "-1";
				}
				else {
					ACCIDENTINDICATOR = String.valueOf(actual.get("ACCIDENTINDICATOR"));
				}
				
				if (actual.get("TICKETISSUEDATE")==null) {
					TICKETISSUEDATE = "-1";
				}
				else {
					 TICKETISSUEDATE = String.valueOf(actual.get("TICKETISSUEDATE"));
				}
				
				if (actual.get("VIOLATIONCODE")==null) {
					VIOLATIONCODE = "-1";
				}
				else {
					VIOLATIONCODE = String.valueOf(actual.get("VIOLATIONCODE"));
				}
				
				if (actual.get("VIOLATIONDESC")==null) {
					VIOLATIONDESC = "-1";
				}
				else {
					 VIOLATIONDESC = String.valueOf(actual.get("VIOLATIONDESC"));
				}

				VOMovingViolations nuevo = new VOMovingViolations(OBJECTID, ROW, LOCATION, ADDRESS_ID, STREETSEGID, XCOORD,
						YCOORD, TICKETTYPE, FINEAMT, TOTALPAID, PENALTY1, PENALTY2, ACCIDENTINDICATOR, TICKETISSUEDATE, VIOLATIONCODE, VIOLATIONDESC);
				cApril++;
				arbolRB.put(Integer.parseInt(OBJECTID), nuevo);

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public void loadMay(String archivoMay) {
		JSONParser parser = new JSONParser();

		try {
			JSONArray a = (JSONArray) parser.parse(new FileReader(archivoMay));

			for (Object o : a) {

				JSONObject actual = (JSONObject) o;
				
				String OBJECTID;
				String ROW;
				String LOCATION;
				String ADDRESS_ID;
				String STREETSEGID;
				String XCOORD;
				String YCOORD;
				String TICKETTYPE;
				String FINEAMT;
				String TOTALPAID;
				String PENALTY1;
				String PENALTY2;
				String ACCIDENTINDICATOR;
				String TICKETISSUEDATE;
				String VIOLATIONCODE;
				String VIOLATIONDESC;
				
				if (actual.get("OBJECTID")==null) {
					OBJECTID = "-1";
				}
				else {
				 OBJECTID = String.valueOf(actual.get("OBJECTID"));
				}
				
				if (actual.get("ROW_")==null) {
					ROW = "-1";
				}
				else {
					 ROW = String.valueOf(actual.get("ROW_"));
				}
				
				if (actual.get("LOCATION")==null) {
					LOCATION = "-1";
				}
				else {
					 LOCATION = String.valueOf(actual.get("LOCATION"));
				}
				
				if (actual.get("ADDRESS_ID")==null) {
					ADDRESS_ID = "-1";
				}
				else {
					ADDRESS_ID = String.valueOf(actual.get("ADDRESS_ID"));
				}
				
				if (actual.get("STREETSEGID")==null) {
					STREETSEGID = "-1";
				}
				else {
					STREETSEGID = String.valueOf(actual.get("STREETSEGID"));
				}
				
				if (actual.get("XCOORD")==null) {
					XCOORD = "-1";
				}
				else {
					 XCOORD = String.valueOf(actual.get("XCOORD"));
				}
				
				if (actual.get("YCOORD")==null) {
					YCOORD = "-1";
				}
				else {
					 YCOORD = String.valueOf(actual.get("YCOORD"));
				}
				
				if (actual.get("TICKETTYPE")==null) {
					TICKETTYPE = "-1";
				}
				else {
					TICKETTYPE = String.valueOf(actual.get("TICKETTYPE"));
				}
				
				if (actual.get("FINEAMT")==null) {
					FINEAMT = "-1";
				}
				else {
					 FINEAMT = String.valueOf(actual.get("FINEAMT"));
				}
				
				if (actual.get("TOTALPAID")==null) {
					TOTALPAID = "-1";
				}
				else {
					TOTALPAID = String.valueOf(actual.get("TOTALPAID"));
				}
				
				if (actual.get("PENALTY1")==null) {
					PENALTY1 = "-1";
				}
				else {
					 PENALTY1 = String.valueOf(actual.get("PENALTY1"));
				}
				
				if (actual.get("PENALTY2")==null) {
					PENALTY2 = "-1";
				}
				else {
					PENALTY2 = String.valueOf(actual.get("PENALTY2"));
				}
				if (actual.get("ACCIDENTINDICATOR")==null) {
					ACCIDENTINDICATOR = "-1";
				}
				else {
					ACCIDENTINDICATOR = String.valueOf(actual.get("ACCIDENTINDICATOR"));
				}
				
				if (actual.get("TICKETISSUEDATE")==null) {
					TICKETISSUEDATE = "-1";
				}
				else {
					 TICKETISSUEDATE = String.valueOf(actual.get("TICKETISSUEDATE"));
				}
				
				if (actual.get("VIOLATIONCODE")==null) {
					VIOLATIONCODE = "-1";
				}
				else {
					VIOLATIONCODE = String.valueOf(actual.get("VIOLATIONCODE"));
				}
				
				if (actual.get("VIOLATIONDESC")==null) {
					VIOLATIONDESC = "-1";
				}
				else {
					 VIOLATIONDESC = String.valueOf(actual.get("VIOLATIONDESC"));
				}

				VOMovingViolations nuevo = new VOMovingViolations(OBJECTID, ROW, LOCATION, ADDRESS_ID, STREETSEGID, XCOORD,
						YCOORD, TICKETTYPE, FINEAMT, TOTALPAID, PENALTY1, PENALTY2, ACCIDENTINDICATOR, TICKETISSUEDATE, VIOLATIONCODE, VIOLATIONDESC);
				cMay++;
				arbolRB.put(Integer.parseInt(OBJECTID), nuevo);

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public void loadJune(String archivoJune) {
		
		JSONParser parser = new JSONParser();

		try {
			JSONArray a = (JSONArray) parser.parse(new FileReader(archivoJune));

			for (Object o : a) {

				JSONObject actual = (JSONObject) o;
				
				String OBJECTID;
				String ROW;
				String LOCATION;
				String ADDRESS_ID;
				String STREETSEGID;
				String XCOORD;
				String YCOORD;
				String TICKETTYPE;
				String FINEAMT;
				String TOTALPAID;
				String PENALTY1;
				String PENALTY2;
				String ACCIDENTINDICATOR;
				String TICKETISSUEDATE;
				String VIOLATIONCODE;
				String VIOLATIONDESC;
				
				if (actual.get("OBJECTID")==null) {
					OBJECTID = "-1";
				}
				else {
				 OBJECTID = String.valueOf(actual.get("OBJECTID"));
				}
				
				if (actual.get("ROW_")==null) {
					ROW = "-1";
				}
				else {
					 ROW = String.valueOf(actual.get("ROW_"));
				}
				
				if (actual.get("LOCATION")==null) {
					LOCATION = "-1";
				}
				else {
					 LOCATION = String.valueOf(actual.get("LOCATION"));
				}
				
				if (actual.get("ADDRESS_ID")==null) {
					ADDRESS_ID = "-1";
				}
				else {
					ADDRESS_ID = String.valueOf(actual.get("ADDRESS_ID"));
				}
				
				if (actual.get("STREETSEGID")==null) {
					STREETSEGID = "-1";
				}
				else {
					STREETSEGID = String.valueOf(actual.get("STREETSEGID"));
				}
				
				if (actual.get("XCOORD")==null) {
					XCOORD = "-1";
				}
				else {
					 XCOORD = String.valueOf(actual.get("XCOORD"));
				}
				
				if (actual.get("YCOORD")==null) {
					YCOORD = "-1";
				}
				else {
					 YCOORD = String.valueOf(actual.get("YCOORD"));
				}
				
				if (actual.get("TICKETTYPE")==null) {
					TICKETTYPE = "-1";
				}
				else {
					TICKETTYPE = String.valueOf(actual.get("TICKETTYPE"));
				}
				
				if (actual.get("FINEAMT")==null) {
					FINEAMT = "-1";
				}
				else {
					 FINEAMT = String.valueOf(actual.get("FINEAMT"));
				}
				
				if (actual.get("TOTALPAID")==null) {
					TOTALPAID = "-1";
				}
				else {
					TOTALPAID = String.valueOf(actual.get("TOTALPAID"));
				}
				
				if (actual.get("PENALTY1")==null) {
					PENALTY1 = "-1";
				}
				else {
					 PENALTY1 = String.valueOf(actual.get("PENALTY1"));
				}
				
				if (actual.get("PENALTY2")==null) {
					PENALTY2 = "-1";
				}
				else {
					PENALTY2 = String.valueOf(actual.get("PENALTY2"));
				}
				if (actual.get("ACCIDENTINDICATOR")==null) {
					ACCIDENTINDICATOR = "-1";
				}
				else {
					ACCIDENTINDICATOR = String.valueOf(actual.get("ACCIDENTINDICATOR"));
				}
				
				if (actual.get("TICKETISSUEDATE")==null) {
					TICKETISSUEDATE = "-1";
				}
				else {
					 TICKETISSUEDATE = String.valueOf(actual.get("TICKETISSUEDATE"));
				}
				
				if (actual.get("VIOLATIONCODE")==null) {
					VIOLATIONCODE = "-1";
				}
				else {
					VIOLATIONCODE = String.valueOf(actual.get("VIOLATIONCODE"));
				}
				
				if (actual.get("VIOLATIONDESC")==null) {
					VIOLATIONDESC = "-1";
				}
				else {
					 VIOLATIONDESC = String.valueOf(actual.get("VIOLATIONDESC"));
				}

				VOMovingViolations nuevo = new VOMovingViolations(OBJECTID, ROW, LOCATION, ADDRESS_ID, STREETSEGID, XCOORD,
						YCOORD, TICKETTYPE, FINEAMT, TOTALPAID, PENALTY1, PENALTY2, ACCIDENTINDICATOR, TICKETISSUEDATE, VIOLATIONCODE, VIOLATIONDESC);
				cJune++;
				arbolRB.put(Integer.parseInt(OBJECTID), nuevo);

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public VOMovingViolations getValueObjectId(int pObjectId)
	{

		return arbolRB.get(pObjectId);
	
		
	}
	
	public IQueue<VOMovingViolations> getObjectIdInRange(int pObjectIdMenor, int pObjectIdMayor)
	{
		IQueue<VOMovingViolations> resp = new Queue<VOMovingViolations>();
		Iterator<Integer> iterator = arbolRB.keys(pObjectIdMenor, pObjectIdMayor).iterator();
		while(iterator.hasNext())
		{
			resp.enqueue(arbolRB.get(iterator.next()));
		}
		return resp;
	}
	
	public RedBlackBalancedSearchTrees<Integer, VOMovingViolations> getTree()
	{
		return arbolRB;
	}
	
	public int getJanuaryCounter()
	{
		return cJanuary;
	}
	
	public int getFebruaryCounter()
	{
		return cFebruary;
	}
	
	public int getMarchCounter()
	{
		return cMarch;
	}
	
	public int getAprilCounter()
	{
		return cApril;
	}
	
	public int getMayCounter()
	{
		return cMay;
	}
	
	public int getJuneCounter()
	{
		return cJune;
	}
	
}
