
package controller;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import org.json.simple.parser.ParseException;

import data_structures.IQueue;
import data_structures.Queue;
import data_structures.RedBlackBalancedSearchTrees;
import logic.MovingViolationsManager;
import sort.Sort;
import view.MovingViolationsManagerView;
import vo.VOMovingViolations;



public class Controller 
{

	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException 
	{
		Controller controler = new Controller();
		controler.run();

	}

	private MovingViolationsManagerView view;

	private MovingViolationsManager manager ;
	
	private RedBlackBalancedSearchTrees<Integer, VOMovingViolations> arbol = new RedBlackBalancedSearchTrees<Integer, VOMovingViolations>();

	public final static String TICKETISSUEDATE = "ticketissuedate";


	public Controller() 
	{
		view = new MovingViolationsManagerView();
		manager = new MovingViolationsManager();
		arbol = manager.getTree();
	}

	public void run() throws FileNotFoundException, IOException, ParseException {
		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		Controller controller = new Controller();

		long startTime;
		long endTime;
		long duration;


		while (!fin) 
		{
			view.printMenu();

			int option = sc.nextInt();

			switch (option) 
			{
			case 0:

				startTime = System.currentTimeMillis();
				manager.loadJanuary("./data/Moving_Violations_Issued_in_January_2018.json");
				manager.loadFebruary("./data/Moving_Violations_Issued_in_February_2018.json");
				manager.loadMarch("./data/Moving_Violations_Issued_in_March_2018.json");
				manager.loadApril("./data/Moving_Violations_Issued_in_April_2018.json");
				manager.loadMay("./data/Moving_Violations_Issued_in_May_2018.json");
				manager.loadJune("./data/Moving_Violations_Issued_in_June_2018.json");
				
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Se cargaron un total de " + arbol.size() + " archivos.");
				view.printMessage("Se cargaron " + manager.getJanuaryCounter() + " archivos de enero.");
				view.printMessage("Se cargaron " + manager.getFebruaryCounter() + " archivos de febrero.");
				view.printMessage("Se cargaron " + manager.getMarchCounter() + " archivos de marzo.");
				view.printMessage("Se cargaron " + manager.getAprilCounter() + " archivos de abril.");
				view.printMessage("Se cargaron " + manager.getMayCounter() + " archivos de mayo.");
				view.printMessage("Se cargaron " + manager.getJuneCounter() + " archivos de junio.");
				view.printMessage("Tiempo de carga: " + duration + " milisegundos");
			
				break;

			case 1:
				view.printMessage("Ingrese el ObjectId a buscar. Ej: 13237805");
				int parametro = sc.nextInt();
				startTime = System.currentTimeMillis();
				VOMovingViolations resultado = manager.getValueObjectId(parametro);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printReq1(resultado);
				view.printMessage("Se demoro " + duration + " milisegundos en buscar la infraccion.");
				break;

			case 2:
				view.printMessage("Ingrese el ObjectId menor. Ej: 13237805");
				int objectIdMenor = sc.nextInt();
				view.printMessage("Ingrese el ObjectId mayor. Ej: 13237812");
				int objectIdMayor = sc.nextInt();
				if(objectIdMenor <= objectIdMayor)
				{
					startTime = System.currentTimeMillis();
					IQueue<VOMovingViolations> resultado2 = manager.getObjectIdInRange(objectIdMenor, objectIdMayor); 
					endTime = System.currentTimeMillis();
					duration = endTime - startTime;
					view.printReq2(resultado2);
					view.printMessage("Se demoro " + duration + " milisegundos en buscar la infracciones.");
				}
				else view.printMessage("El objectId menor tiene que ser menor o igual al objectId mayor.");
				break;
			case 3:
				fin = true;
				sc.close();
				break;
			}
		}

	}
	
	
}
